import { Component, OnInit } from '@angular/core';
import { User } from '../IUser';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  user: User = {
    id: 1,
    name: "John",
    age: 22,
    email: "jhonedoe@e.com"
  };

  users: User[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(user: User): void {
    this.user = user;
  }

  onClick(): void {
    this.users = [{
      id: 1,
      name: "John",
      age: 22,
      email: "jhonedoe@e.com"
    }, {
      id: 2,
      name: "Andrew",
      age: 23,
      email: "andrew@e.com"
    }, {
      id: 3,
      name: "Alin",
      age: 22,
      email: "alin@e.com"
    }];
  }
}
